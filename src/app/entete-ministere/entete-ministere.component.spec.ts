import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnteteMinistereComponent } from './entete-ministere.component';

describe('EnteteMinistereComponent', () => {
  let component: EnteteMinistereComponent;
  let fixture: ComponentFixture<EnteteMinistereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnteteMinistereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnteteMinistereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
