import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeIsepComponent } from './home-isep.component';

describe('HomeIsepComponent', () => {
  let component: HomeIsepComponent;
  let fixture: ComponentFixture<HomeIsepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeIsepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeIsepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
