import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../models/article';
import {Event} from '../models/event';
import {ArticleService} from '../services/article.service';
import {AppelDOffreService} from '../services/appel-d-offre.service';
import {EventService} from '../services/event.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSmartModalComponent, NgxSmartModalService} from 'ngx-smart-modal';
import {AllocutionService} from '../services/allocution.service';
import {Allocution} from '../models/allocution';

@Component({
  selector: 'app-home-isep',
  templateUrl: './home-isep.component.html',
  styleUrls: ['./home-isep.component.css'],
  providers: [ArticleService, EventService, AppelDOffreService, AllocutionService]
})
export class HomeIsepComponent implements OnInit {
  @Output()back: EventEmitter<Article> = new EventEmitter();
  a: Article;
  articles = [];
  article: Article;
  event: Event;
  allocutions = [];
  allocution: Allocution;
  profil = '../../assets/img/person_2.jpg';
  constructor(private articleService: ArticleService,
              private eventService: EventService,
              private route: ActivatedRoute,
              private router: Router,
  public ngxSmartModalService: NgxSmartModalService,
              private allocutionService: AllocutionService) { }

  ngOnInit() {
    this.article = new Article();
    this.articles = [];
    this.getArticles();
    console.log('After getArticles()');
    this.getAnEvent();
    this.allocution = new Allocution();
    this.allocutions = this.allocutionService.allocutions;
    this.getAllocutions();
  }
  getArticles(): void {
    this.articleService.getArticlesPublies().subscribe(
      articles => {
        this.articles = articles;
        console.log(this.articles);
      },
      error => {
        console.log(error);
      }
    );
  }
  onVisualiserArticle(art: Article) {
    art.vues = art.vues + 1;
    console.log(art.vues);
    this.articleService.editArticle(art).subscribe(
      article => {
        this.back.emit(article);

        this.router.navigate(['showarticle/' + art.id]);


      }
    );
  }
  private getAnEvent() {
    this.eventService.getEventsPublies().subscribe(
      events => {
        this.event = events[0];
      },
      error => {
        console.log(error);
      }
    );
  }
  private getAllocutions() {
    this.allocutionService.getRandomAllocution().subscribe(
      allocution => {
        allocution.video = [];
        if (allocution.media.type === 'mp4' || allocution.media.type === 'ogg' || allocution.media.type === 'webm') {
          allocution.video = allocution.media;
        }
        this.allocution = allocution;
        console.log(allocution);
      },
      error => {
        console.log(error);
      }
    );
  }
}

