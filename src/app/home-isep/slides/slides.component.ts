import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {AppelDOffre} from '../../models/appel-d-offre';
import {AppelDOffreService} from '../../services/appel-d-offre.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.css'],
  providers: [AppelDOffreService]
})
export class SlidesComponent implements OnInit {

  a: AppelDOffre;
  appelsDoffres = [];
  appel: AppelDOffre;
  constructor(private appelDoffreService: AppelDOffreService, private articleService: ArticleService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.appel = new AppelDOffre();
    this.appelsDoffres = this.appelDoffreService.appelDOffres;
    this.getArticles();
    console.log('After getArticles()');
  }
  private getArticles() {
    this.appelDoffreService.getAppelDOffresPublies().subscribe(
      articles => {
        this.appelsDoffres = articles;
        console.log(this.appelsDoffres);
        },
      error => {
        console.log(error);
      }
    );
  }
}

