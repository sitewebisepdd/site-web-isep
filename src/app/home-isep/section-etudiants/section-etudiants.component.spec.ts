import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionEtudiantsComponent } from './section-etudiants.component';

describe('SectionEtudiantsComponent', () => {
  let component: SectionEtudiantsComponent;
  let fixture: ComponentFixture<SectionEtudiantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionEtudiantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionEtudiantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
