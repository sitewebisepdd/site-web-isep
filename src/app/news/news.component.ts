import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {Article} from '../models/article';
import {ArticleService} from '../services/article.service';
import {isUndefined} from 'util';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  providers: [ArticleService]
})
export class NewsComponent implements OnInit {
  @Output()back: EventEmitter<Article> = new EventEmitter();
  a: Article;
  articles = [];
  articlesb = [];
  article1: Article;
  art: Article;
  article: Article;
  constructor(private articleService: ArticleService, private router: Router ) { }

  ngOnInit() {
    this.article = new Article();
    this.articles = this.articleService.articles;
    this.getArticles();
    this.getMostShowedArticles();
    console.log('After getArticles()');
  }
  onVisualiserArticle(art: Article) {
    art.vues = art.vues + 1;
    console.log(art.vues);
    this.articleService.editArticle(art).subscribe(
      article => {
        this.back.emit(article);
        this.router.navigate(['showarticle/' + art.id]);


      }
    );
  }
  private getArticles() {
    this.articleService.getArticlesPublies().subscribe(
      articles => {
        for (let j = 0 ; j < articles.length ; j++) {
          articles[j].images = [];
          articles[j].videos = [];
          articles[j].documents = [];
          this.articleService.getMediasByArticle(articles[j].id).subscribe(
                medias => {
                  for (let i = 0; i < medias.length; i++) {
                    if (medias[i].type === 'PNG' || medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                      articles[j].images.push(medias[i]);
                    }
                    if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                      articles[j].videos.push(medias[i]);
                    }
                    if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                      articles[j].documents.push(medias[i]);
                    }
                  }
                }
          );
        }
        this.articles = articles;
        this.article1 = articles[0];
        console.log(this.articles);
      },
      error => {
        console.log(error);
      }
    );
  }
  private getMostShowedArticles() {
    this.articleService.getArticlesPublies().subscribe(
      articles => {
        for (let j = 0 ; j < articles.length ; j++) {
          articles[j].images = [];
          articles[j].videos = [];
          articles[j].documents = [];
          this.articleService.getMediasByArticle(articles[j].id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'PNG' || medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                  articles[j].images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'webm' || medias[i].type === 'ogg') {
                  articles[j].videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  articles[j].documents.push(medias[i]);
                }
              }
            }
          );
          for (let k = 0 ; k < articles.length ; k++) {
            if (articles[k].vues < articles[j].vues ) {
              this.art = articles[k];
              articles[k] = articles[j];
              articles[j] = this.art;
            }
          }
        }
        this.articlesb = articles;
        console.log(this.articlesb);
      },
      error => {
        console.log(error);
      }
    );
  }
}


