import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NosEtudiantsComponent } from './nos-etudiants.component';

describe('NosEtudiantsComponent', () => {
  let component: NosEtudiantsComponent;
  let fixture: ComponentFixture<NosEtudiantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NosEtudiantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NosEtudiantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
