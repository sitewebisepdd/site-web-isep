import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainBrother2Component } from './contain-brother2.component';

describe('ContainBrother2Component', () => {
  let component: ContainBrother2Component;
  let fixture: ComponentFixture<ContainBrother2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainBrother2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainBrother2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
