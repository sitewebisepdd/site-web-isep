import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ArticleService} from '../services/article.service';
import {Article} from '../models/article';
import {Router} from '@angular/router';

@Component({
  selector: 'app-contain-brother2',
  templateUrl: './contain-brother2.component.html',
  styleUrls: ['./contain-brother2.component.css'],
  providers: [ArticleService]
})
export class ContainBrother2Component implements OnInit {
  @Output()back: EventEmitter<Article> = new EventEmitter();
  article: Article;
  a: Article;
  @Input() articles: Article[];
  constructor(private articleService: ArticleService, private router: Router) {}
  ngOnInit() {
    this.getArticles();
  }
  onVisualiserArticle(art: Article) {
    art.vues = art.vues + 1;
    console.log(art.vues);
    this.articleService.editArticle(art).subscribe(
      article => {
        this.back.emit(article);
        window.location.reload(true);
        this.router.navigate(['showarticle/' + art.id]);


      }
    );
  }
getArticles(): void {
  this.articleService.getNews().subscribe(
    articles => {
      this.articles = articles;
      console.log(this.articles);
    },
    error => {
      console.log(error);
    }
  );
}
}
