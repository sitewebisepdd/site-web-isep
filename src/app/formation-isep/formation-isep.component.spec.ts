import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormationIsepComponent } from './formation-isep.component';

describe('FormationIsepComponent', () => {
  let component: FormationIsepComponent;
  let fixture: ComponentFixture<FormationIsepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormationIsepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormationIsepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
