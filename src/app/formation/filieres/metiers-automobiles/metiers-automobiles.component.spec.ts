import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetiersAutomobilesComponent } from './metiers-automobiles.component';

describe('MetiersAutomobilesComponent', () => {
  let component: MetiersAutomobilesComponent;
  let fixture: ComponentFixture<MetiersAutomobilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetiersAutomobilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetiersAutomobilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
