import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetiersDuTicComponent } from './metiers-du-tic.component';

describe('MetiersDuTicComponent', () => {
  let component: MetiersDuTicComponent;
  let fixture: ComponentFixture<MetiersDuTicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetiersDuTicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetiersDuTicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
