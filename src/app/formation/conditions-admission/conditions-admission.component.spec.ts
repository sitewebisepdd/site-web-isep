import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditionsAdmissionComponent } from './conditions-admission.component';

describe('ConditionsAdmissionComponent', () => {
  let component: ConditionsAdmissionComponent;
  let fixture: ComponentFixture<ConditionsAdmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConditionsAdmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionsAdmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
