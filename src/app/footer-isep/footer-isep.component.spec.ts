import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterIsepComponent } from './footer-isep.component';

describe('FooterIsepComponent', () => {
  let component: FooterIsepComponent;
  let fixture: ComponentFixture<FooterIsepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterIsepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterIsepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
