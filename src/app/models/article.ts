import {ArticleTag} from './article-tag';
import {MediaArticle} from './media-article';
import {Media} from './media';
import {Partenaire} from './partenaire';


export class Article {
  id: number;
  titre: string;
  corps: string;
  dateCreation: Date;
  images: Media[];
  videos: Media[];
  documents: Media[];
  datePublication: Date;
  mediaArticles: MediaArticle[];
  state: boolean;
  articleTags: ArticleTag[];
  medias: Media;
  partenaire: Partenaire;
  pageCible: string;
  vues: number;
}
