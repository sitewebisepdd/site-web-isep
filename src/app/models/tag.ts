import {ArticleTag} from './article-tag';

export class Tag {
 id: number;
 nom: string;
 articles: ArticleTag[];
}
