import {Article} from './article';
import {Media} from './media';

export class MediaArticlePk {
  article: Article;
  media: Media;
}
