import {Filiere} from './filiere';
import {User} from './user';

export class Departement {
  id: number;
  nom: string;
  description: string;
  filieres: Filiere[];
  user: User;
}
