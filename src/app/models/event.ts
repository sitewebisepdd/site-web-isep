import {Article} from './article';
import {Media} from './media';

export class Event extends Article {
  dateDebut: Date;
  dateFin: Date;
  lieu: string;
  type: string;
  nextEvent: Event;
}
