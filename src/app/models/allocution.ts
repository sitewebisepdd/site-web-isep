import {Media} from './media';

export class Allocution {
  id: number;
  titre: String;
  corps: String;
  media: Media;
  video: Media;
}
