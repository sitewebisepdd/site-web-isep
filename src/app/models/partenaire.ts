import {User} from './user';
import {Media} from './media';
import {Article} from './article';

export class Partenaire {
  id: number;
  nom: string;
  siteWeb: string;
  descriptionPartenariat: string;
  media: Media;
  user: User;
  articles: Article[];
}
