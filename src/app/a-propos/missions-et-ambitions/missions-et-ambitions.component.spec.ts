import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsEtAmbitionsComponent } from './missions-et-ambitions.component';

describe('MissionsEtAmbitionsComponent', () => {
  let component: MissionsEtAmbitionsComponent;
  let fixture: ComponentFixture<MissionsEtAmbitionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionsEtAmbitionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsEtAmbitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
