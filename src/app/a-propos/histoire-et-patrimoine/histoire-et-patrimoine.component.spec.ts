import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoireEtPatrimoineComponent } from './histoire-et-patrimoine.component';

describe('HistoireEtPatrimoineComponent', () => {
  let component: HistoireEtPatrimoineComponent;
  let fixture: ComponentFixture<HistoireEtPatrimoineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoireEtPatrimoineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoireEtPatrimoineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
