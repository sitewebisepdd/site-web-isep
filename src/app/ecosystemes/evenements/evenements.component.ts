import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {EventService} from '../../services/event.service';
import {Event} from '../../models/event';

@Component({
  selector: 'app-evenements',
  templateUrl: './evenements.component.html',
  styleUrls: ['./evenements.component.css'],
  providers: [EventService,
    ArticleService]
})
export class EvenementsComponent implements OnInit {
  e: Event;
  events = [];
  event: Event;
  constructor(private eventService: EventService, private articleService: ArticleService) { }

  ngOnInit() {
    this.event = new Event();
    this.events = this.eventService.events;
    this.getEvents();
    console.log('After getArticles()');
  }
  private getEvents() {
    this.eventService.getEventsPublies().subscribe(
      articles => {
        for (let j = 0 ; j < articles.length ; j++) {
          articles[j].images = [];
          articles[j].videos = [];
          articles[j].documents = [];
          articles[j].nextEvent = articles[j + 1];
          this.articleService.getMediasByArticle(articles[j].id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'PNG' || medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                  articles[j].images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                  articles[j].videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  articles[j].documents.push(medias[i]);
                }
              }
            }
          );
        }
        this.events = articles;
        console.log(this.events);
      },
      error => {
        console.log(error);
      }
    );
  }
}


