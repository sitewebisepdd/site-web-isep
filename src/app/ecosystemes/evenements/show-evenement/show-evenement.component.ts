import { Component, OnInit } from '@angular/core';
import {Event} from '../../../models/event';
import {EventService} from '../../../services/event.service';
import {ArticleService} from '../../../services/article.service';

@Component({
  selector: 'app-show-evenement',
  templateUrl: './show-evenement.component.html',
  styleUrls: ['./show-evenement.component.css'],
  providers: [EventService,
    ArticleService]
})
export class ShowEvenementComponent implements OnInit {
  e: Event;
  events = [];
  event: Event;
  constructor(private eventService: EventService, private articleService: ArticleService) { }

  ngOnInit() {
    this.event = new Event();
    this.events = [];
    this.getEvents();
    console.log('After getArticles()');
  }
  private getEvents() {
    this.eventService.getEventsPublies().subscribe(
      articles => {
        for (let j = 0 ; j < articles.length ; j++) {
          articles[j].images = [];
          articles[j].videos = [];
          articles[j].documents = [];
          articles[j].nextEvent = articles[j + 1];
          this.articleService.getMediasByArticle(articles[j].id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'PNG' || medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                  articles[j].images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                  articles[j].videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  articles[j].documents.push(medias[i]);
                }
              }
            }
          );
        }
        this.events = articles;
        console.log(this.events);
      },
      error => {
        console.log(error);
      }
    );
  }
}


