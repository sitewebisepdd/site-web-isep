import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogosPartenaireComponent } from './logos-partenaire.component';

describe('LogosPartenaireComponent', () => {
  let component: LogosPartenaireComponent;
  let fixture: ComponentFixture<LogosPartenaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogosPartenaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogosPartenaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
