import { Component, OnInit } from '@angular/core';
import {Partenaire} from '../../models/partenaire';
import {PartenaireService} from '../../services/partenaire.service';

@Component({
  selector: 'app-logos-partenaire',
  templateUrl: './logos-partenaire.component.html',
  styleUrls: ['./logos-partenaire.component.css'],
  providers: [PartenaireService]
})
export class LogosPartenaireComponent implements OnInit {p: Partenaire;
  partenaires: Partenaire[];
  constructor(private partenaireService: PartenaireService) { }

  ngOnInit() {

    this.getpartenaires();
  }
  private getpartenaires() {
    this.partenaires = [];
    this.partenaireService.getPartenaires().subscribe(
      partenaires => {
        for (let i = 0; i < partenaires.length; i++) {
          this.partenaires.push(partenaires[i]);
        }
        for (let i = 0; i < partenaires.length; i++) {
          this.partenaires.push(partenaires[i]);
        }
        for (let i = 0; i < partenaires.length; i++) {
          this.partenaires.push(partenaires[i]);
        }
        console.log(this.partenaires);
      },
      errorr => {
        console.log(errorr);
      }
    );
  }
}
