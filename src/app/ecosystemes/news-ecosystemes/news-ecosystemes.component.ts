import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ArticleService} from '../../services/article.service';
import {Article} from '../../models/article';
import {Router} from '@angular/router';

@Component({
  selector: 'app-news-ecosystemes',
  templateUrl: './news-ecosystemes.component.html',
  styleUrls: ['./news-ecosystemes.component.css'],
  providers: [ArticleService]
})
export class NewsEcosystemesComponent implements OnInit {
  articles: Article[];
  articleLesPlusLus: Article[];
  art: Article;
  @Output()back: EventEmitter<Article> = new EventEmitter();
  pageSize = 10;
  pageNumber = 1;
  gettingData: boolean;
  editingArticle: boolean;
  constructor(private articleService: ArticleService,
              private router: Router) { }

  ngOnInit() {
    this.articles = [];
    this.articleLesPlusLus = [];
    this.getArticles();
    this.getMostShowedArticles();
  }
  getMostShowedArticles() {
    this.gettingData = true;
    this.articleService.getArticlesPublies().subscribe(
      articles => {
        for (let j = 0 ; j < articles.length ; j++) {
          articles[j].images = [];
          articles[j].videos = [];
          articles[j].documents = [];
          this.articleService.getMediasByArticle(articles[j].id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                  articles[j].images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'webm' || medias[i].type === 'ogg') {
                  articles[j].videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  articles[j].documents.push(medias[i]);
                }
              }
            }
          );
          for (let k = 0 ; k < articles.length ; k++) {
            if (articles[k].vues < articles[j].vues ) {
              this.art = articles[k];
              articles[k] = articles[j];
              articles[j] = this.art;
            }
          }
        }
        this.articleLesPlusLus = articles;
        console.log(this.articleLesPlusLus);
        this.gettingData = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  onVoirArt(article: Article) {
    article.vues = article.vues + 1;
    console.log(article.vues);
    this.editingArticle = true;
    this.articleService.editArticle(article).subscribe(
      art => {
        this.back.emit(art);
        article = art;
        this.editingArticle = false;
        if (article.pageCible === 'eco') {
          this.router.navigate(['ecosystemes/show-article-eco/' + article.id]);
        } else {
          this.router.navigate(['showarticle/' + article.id]);
        }
      },
        err => {
        console.log(err);
      }
    );
  }
  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }

  getArticles() {
    this.gettingData = true;
    this.articleService.getArticlesPubliesByPage('eco').subscribe(
      articles => {
        this.articles = articles;
        console.log(this.articles);
        for (let j = 0 ; j < this.articles.length ; j++) {
          this.articles[j].images = [];
          this.articles[j].videos = [];
          this.articles[j].documents = [];
          this.articleService.getMediasByArticle(this.articles[j].id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'png' || medias[i].type === 'PNG' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                  this.articles[j].images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                  this.articles[j].videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  this.articles[j].documents.push(medias[i]);
                }
              }
            }
          );
        }
        console.log('LES AUTRES');
        console.log(this.articles);
        this.gettingData = false;
      },
      err => {
        console.log(err);
      }
    );
  }
}
