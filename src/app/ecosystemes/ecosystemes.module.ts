import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import {EcosystemesComponent} from './ecosystemes.component';
import {AppRoutingModule} from '../app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
import {ServeurService} from '../services/serveur.service';
import {AppComponent} from '../app.component';
import {ShowpartenaireComponent} from './showpartenaire/showpartenaire.component';
import { LogosPartenaireComponent } from './logos-partenaire/logos-partenaire.component';
import { HeaderEcosystemesComponent } from './header-ecosystemes/header-ecosystemes.component';
import { ContactUsComponent } from './contact-us/contact-us.component';





@NgModule({
  declarations: [

 ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxSmartModalModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_KEY'
    })
  ],
  providers: [ServeurService, NgxSmartModalService],
  bootstrap: [AppComponent]
})
export class EcosystemesModule { }
