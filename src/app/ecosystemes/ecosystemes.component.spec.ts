import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcosystemesComponent } from './ecosystemes.component';

describe('EcosystemesComponent', () => {
  let component: EcosystemesComponent;
  let fixture: ComponentFixture<EcosystemesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcosystemesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcosystemesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
