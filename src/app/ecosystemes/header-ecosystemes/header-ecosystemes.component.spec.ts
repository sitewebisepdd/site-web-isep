import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderEcosystemesComponent } from './header-ecosystemes.component';

describe('HeaderEcosystemesComponent', () => {
  let component: HeaderEcosystemesComponent;
  let fixture: ComponentFixture<HeaderEcosystemesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderEcosystemesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderEcosystemesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
