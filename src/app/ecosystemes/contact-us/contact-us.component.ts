import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MessageService} from '../../services/message.service';
import {Message} from '../../models/message';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['../ecosystemes.component.css'],
  providers: [MessageService]
})
export class ContactUsComponent implements OnInit {
  submitting: boolean;
  formMessage: FormGroup;
  @Output()back: EventEmitter<Message> = new EventEmitter();
  m: Message;
  alerte: string;
  messages: Message[];
  message: Message;
  feedBack(mes: Message): void {
    this.messages.push(mes);
  }
  ngOnInit(): void {
    this.message = new Message();
    /*this.users = this.userService.users;
    this.getUsers();*/

  }

  constructor(private fB: FormBuilder,
              private messageService: MessageService,
              private router: Router
  ) {
    this.submitting = false;
    this.m = new Message();
    this.formMessage = this.fB.group({
      'corps': ['', Validators.required],
      'emailEmetteur': ['', Validators.required],
      'nameEmetteur': [''],
      'phoneEmetteur': [''],
      'companyEmetteur': [''],
    });

  }

  onSubmit() {
    this.submitting = true;
    if (this.m.id === undefined) {
      this.m.objet = 'Message visiteur du site';
      this.m.emailDestinataire = 'isepdd2018@gmail.com';
      this.m.lu = false;
      this.m.received = true;
      console.log(this.m.corps);
      console.log(this.m.emailEmetteur);
      this.messageService.createMessage(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.submitting = false;
          this.alerte = 'Envoi réussi!!!';
          setTimeout(
            () => this.alerte = undefined,
            2000
          );
          this.router.navigate(['ecosystemes']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.messageService.editMessage(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.submitting = false;
          this.alerte = 'Envoi réussi!';
          setTimeout(
            () => this.alerte = undefined,
            2000
          );
          this.router.navigate(['ecosystemes']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

}
