import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowpartenaireComponent } from './showpartenaire.component';

describe('ShowpartenaireComponent', () => {
  let component: ShowpartenaireComponent;
  let fixture: ComponentFixture<ShowpartenaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowpartenaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowpartenaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
