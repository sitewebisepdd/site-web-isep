import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {ActivatedRoute} from '@angular/router';
import {PartenaireService} from '../../services/partenaire.service';
import {Partenaire} from '../../models/partenaire';

@Component({
  selector: 'app-showpartenaire',
  templateUrl: './showpartenaire.component.html',
  styleUrls: ['./showpartenaire.component.css'],
  providers: [PartenaireService, ArticleService]
})
export class ShowpartenaireComponent implements OnInit {

  @Output() back: EventEmitter<Article> = new EventEmitter();
  article: Article;
  a: Article;
  p: Partenaire;
  partenaire: Partenaire;
  articles: Article[];
  partenaires: Partenaire[];
  idd: number;
  constructor(private articleService: ArticleService,
              private partenaireService: PartenaireService,
              private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.idd = this.route.snapshot.params['id'];
    this.getPartenaire();
    this.partenaireService.getPartenaires().subscribe(
      parts => {
        this.partenaires = parts;
        console.log(this.partenaires);
      },
      err => {console.log(err);}
    );
  }
  getPartenaire(): void {
    this.partenaireService.getPartenaireById(this.route.snapshot.params['id']).subscribe(
      p => {
        this.partenaire = p;
        document.getElementById('text').innerHTML = this.partenaire.descriptionPartenariat;
        console.log(this.partenaire);
        this.articleService.getArticlesByPartenaire(this.partenaire).subscribe(
          articles => {
            this.articles = articles;
            console.log(this.articles);
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );
  }
}

