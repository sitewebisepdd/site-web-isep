import { Component, OnInit } from '@angular/core';
import {PartenaireService} from '../../services/partenaire.service';
import {Partenaire} from '../../models/partenaire';
import {error} from 'selenium-webdriver';

@Component({
  selector: 'app-partenaires',
  templateUrl: './partenaires.component.html',
  styleUrls: ['./partenaires.component.css'],
  providers: [PartenaireService]
})
export class PartenairesComponent implements OnInit {
  p: Partenaire;
  partenaires: Partenaire[];
  constructor(private partenaireService: PartenaireService) { }

  ngOnInit() {

    this.getpartenaires();
  }
  private getpartenaires() {
   this.partenaireService.getPartenaires().subscribe(
     partenaires => {
       this.partenaires = partenaires;
       console.log(this.partenaires);
     },
     errorr => {
       console.log(errorr);
     }
   );
  }
}
