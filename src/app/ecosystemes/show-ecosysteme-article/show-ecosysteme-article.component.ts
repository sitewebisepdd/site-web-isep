import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../../services/article.service';
import {Article} from '../../models/article';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-show-ecosysteme-article',
  templateUrl: './show-ecosysteme-article.component.html',
  styleUrls: ['./show-ecosysteme-article.component.css'],
  providers: [ArticleService]
})
export class ShowEcosystemeArticleComponent implements OnInit {
  article: Article;
  constructor(private articleService: ArticleService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.articleService.getArticleById(this.route.snapshot.params['id']).subscribe(
      a => {
        a.images = [];
        a.videos = [];
        a.documents = [];
        this.articleService.getMediasByArticle(a.id).subscribe(
          medias => {
            for (let i = 0; i < medias.length; i++) {
              if (medias[i].type === 'PNG' || medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                a.images.push(medias[i]);
              }
              if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                a.videos.push(medias[i]);
              }
              if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                a.documents.push(medias[i]);
              }
            }
          }
        );
        this.article = a;
        document.getElementById('texte').innerHTML = a.corps;
      }, err => {console.log(err); }
    );
  }

}
