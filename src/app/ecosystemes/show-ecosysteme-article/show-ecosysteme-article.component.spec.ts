import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowEcosystemeArticleComponent } from './show-ecosysteme-article.component';

describe('ShowEcosystemeArticleComponent', () => {
  let component: ShowEcosystemeArticleComponent;
  let fixture: ComponentFixture<ShowEcosystemeArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowEcosystemeArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowEcosystemeArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
