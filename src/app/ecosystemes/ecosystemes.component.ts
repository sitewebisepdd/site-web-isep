import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../models/article';
import {ArticleService} from '../services/article.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-ecosystemes',
  templateUrl: './ecosystemes.component.html',
  styleUrls: ['./ecosystemes.component.css'],
  providers: [ArticleService]
})
export class EcosystemesComponent implements OnInit {
  a: Article;
  articles: Article[];
  article1: Article;
  article: Article;
  @Output()back: EventEmitter<Article> = new EventEmitter();
  // getttingData: boolean;
  gettingData: boolean;
  constructor(private articleService: ArticleService,
              private router: Router) { }

  ngOnInit() {
    this.article = new Article();
    this.articles = [];
    this.getArticles();
    console.log('After getArticles()');
  }
 getArticles() {
    this.gettingData = true;
    this.articleService.getArticlesPubliesByPage('eco').subscribe(
      articles => {
        for (let j = 0 ; j < articles.length ; j++) {
          articles[j].images = [];
          articles[j].videos = [];
          articles[j].documents = [];
          this.articleService.getMediasByArticle(articles[j].id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'png' || medias[i].type === 'PNG' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                  articles[j].images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                  articles[j].videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  articles[j].documents.push(medias[i]);
                }
              }
            }
          );
        }
        this.articles = articles;
        this.article1 = this.articles[0];
        console.log(this.articles);
        this.gettingData = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  onVoirArticle(article: Article): void {
    article.vues = article.vues + 1;
    this.articleService.editArticle(article).subscribe(
      artic => {
        this.back.emit(artic);
        article = artic;
        this.router.navigate(['ecosystemes/show-article-eco/' + article.id ]);
      }, err => {console.log(err); }
    );
  }
}


