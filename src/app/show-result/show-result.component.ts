import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../models/article';
import {ArticleService} from '../services/article.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-show-result',
  templateUrl: './show-result.component.html',
  styleUrls: ['./show-result.component.css'],
  providers: [ArticleService]
})
export class ShowResultComponent implements OnInit {

  @Output() back: EventEmitter<Article> = new EventEmitter();
  article: Article;
  a: Article;
  art: Article;
  articlesb = [];
  articlesTrouves: Article[];
  tagName: string;
  constructor(private articleService: ArticleService,
              private route: ActivatedRoute,
              private router: Router) {this.articlesTrouves = [];}
  ngOnInit(): void {
    this.tagName = this.route.snapshot.params['tagName'];
    this.getArticles();
    this.getMostShowedArticles();
  }
  onVisualiserArticle(art: Article) {
    art.vues = art.vues + 1;
    console.log(art.vues);
    this.articleService.editArticle(art).subscribe(
      article => {
        this.back.emit(article);

        this.router.navigate(['showarticle/' + art.id]);


      }
    );
  }
  private getMostShowedArticles() {
    this.articleService.getNews().subscribe(
      articles => {
        for (let j = 0 ; j < articles.length ; j++) {
          articles[j].images = [];
          articles[j].videos = [];
          articles[j].documents = [];
          this.articleService.getMediasByArticle(articles[j].id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'PNG' || medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                  articles[j].images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'webm' || medias[i].type === 'ogg') {
                  articles[j].videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  articles[j].documents.push(medias[i]);
                }
              }
            }
          );
          for (let k = 0 ; k < articles.length ; k++) {
            if (articles[k].vues < articles[j].vues ) {
              this.art = articles[k];
              articles[k] = articles[j];
              articles[j] = this.art;
            }
          }
        }
        this.articlesb = articles;
        console.log(this.articlesb);
      },
      error => {
        console.log(error);
      }
    );
  }
  private getArticles() {
    this.articleService.getArticlesByTag(this.tagName).subscribe(
      articles => {
        for (let j = 0 ; j < articles.length ; j++) {
          articles[j].images = [];
          articles[j].videos = [];
          articles[j].documents = [];
          this.articleService.getMediasByArticle(articles[j].id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'PNG' || medias[i].type === 'jpg' ||  medias[i].type === 'png' || medias[i].type === 'jpeg') {
                  articles[j].images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                  articles[j].videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  articles[j].documents.push(medias[i]);
                }
              }
            }
          );
        }
        this.articlesTrouves = articles;
        console.log(this.articlesTrouves);
      },
      error => {
        console.log(error);
      }
    );
  }
}
