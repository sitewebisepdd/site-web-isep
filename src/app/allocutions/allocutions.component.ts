import { Component, OnInit } from '@angular/core';
import {Allocution} from '../models/allocution';
import {AllocutionService} from '../services/allocution.service';

@Component({
  selector: 'app-allocutions',
  templateUrl: './allocutions.component.html',
  styleUrls: ['./allocutions.component.css'],
  providers: [AllocutionService]
})
export class AllocutionsComponent implements OnInit {

  a: Allocution;
  allocutions = [];
  allocution: Allocution;
  constructor(private allocutionService: AllocutionService ) { }

  ngOnInit() {
    this.allocution = new Allocution();
    this.allocutions = this.allocutionService.allocutions;
    this.getAllocutions();
    console.log('After getAlocutions()');
  }
  private getAllocutions() {
    this.allocutionService.getRandomAllocution().subscribe(
      allocution => {
        this.allocution = allocution;
        console.log(this.allocutionService);
      },
      error => {
        console.log(error);
      }
    );
  }
}


