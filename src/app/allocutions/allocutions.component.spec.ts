import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocutionsComponent } from './allocutions.component';

describe('AllocutionsComponent', () => {
  let component: AllocutionsComponent;
  let fixture: ComponentFixture<AllocutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocutionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
