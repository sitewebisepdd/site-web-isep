import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Article} from '../models/article';
import {ArticleService} from '../services/article.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-contain-brother',
  templateUrl: './contain-brother.component.html',
  styleUrls: ['./contain-brother.component.css'],
  providers: [ArticleService]
})
export class ContainBrotherComponent implements OnInit {
  @Output()back: EventEmitter<Article> = new EventEmitter();
  article: Article;
  a: Article;
  @Input() articles: Article[];
  constructor(private articleService: ArticleService, private router: Router) {}
  ngOnInit() {
    this.getArticles();
  }
  onVisualiserArticle(art: Article) {
    art.vues = art.vues + 1;
    console.log(art.vues);
    this.articleService.editArticle(art).subscribe(
      article => {
        this.back.emit(article);
        window.location.reload(true);
        this.router.navigate(['showarticle/' + art.id]);


      }
    );
  }
  getArticles(): void {
    this.articleService.getNews().subscribe(
      articles => {
        for (let j = 0 ; j < articles.length ; j++) {
          articles[j].images = [];
          articles[j].videos = [];
          articles[j].documents = [];
          this.articleService.getMediasByArticle(articles[j].id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'PNG' || medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                  articles[j].images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                  articles[j].videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  articles[j].documents.push(medias[i]);
                }
              }
            }
          );
        }
        this.articles = articles;
        console.log(this.articles);
      },
      error => {
        console.log(error);
      }
    );
  }
}
