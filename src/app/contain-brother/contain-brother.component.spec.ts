import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainBrotherComponent } from './contain-brother.component';

describe('ContainBrotherComponent', () => {
  let component: ContainBrotherComponent;
  let fixture: ComponentFixture<ContainBrotherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainBrotherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainBrotherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
