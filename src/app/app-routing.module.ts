import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeIsepComponent} from './home-isep/home-isep.component';
import {NousConnaitreComponent} from './a-propos/nous-connaitre/nous-connaitre.component';
import {MissionsEtAmbitionsComponent} from './a-propos/missions-et-ambitions/missions-et-ambitions.component';
import {HistoireEtPatrimoineComponent} from './a-propos/histoire-et-patrimoine/histoire-et-patrimoine.component';
import {OrganigrammeComponent} from './a-propos/organigramme/organigramme.component';
import {ConditionsAdmissionComponent} from './formation/conditions-admission/conditions-admission.component';
import {ListeAdmisComponent} from './formation/liste-admis/liste-admis.component';
import {MetiersAutomobilesComponent} from './formation/filieres/metiers-automobiles/metiers-automobiles.component';
import {MetiersDuTicComponent} from './formation/filieres/metiers-du-tic/metiers-du-tic.component';
import {NosEtudiantsComponent} from './nos-etudiants/nos-etudiants.component';
import {AlumniComponent} from './alumni/alumni.component';
import {EcosystemesComponent} from './ecosystemes/ecosystemes.component';
import {NewsComponent} from './news/news.component';
import {EspaceAdminComponent} from './espace-admin/espace-admin.component';
import {ShowArticleComponent} from './show-article/show-article.component';
import {ShowDepartementComponent} from './show-departement/show-departement.component';
import {ShowFiliereComponent} from './show-departement/show-filiere/show-filiere.component';
import {FormationIsepComponent} from './formation-isep/formation-isep.component';
import {FirstHeaderComponent} from './first-header/first-header.component';
import {NewHeaderComponent} from './new-header/new-header.component';
import {AllocutionsComponent} from './allocutions/allocutions.component';
import {CertificationsComponent} from './formation/certifications/certifications.component';
import {PartenairesComponent} from './ecosystemes/partenaires/partenaires.component';
import {EvenementsComponent} from './ecosystemes/evenements/evenements.component';
import {AppelsDOffresComponentComponent} from './appels-doffres-component/appels-doffres-component.component';
import {ShoweventsComponent} from './showevents/showevents.component';
import {ShowpartenaireComponent} from './ecosystemes/showpartenaire/showpartenaire.component';
import {ContactUsComponent} from './ecosystemes/contact-us/contact-us.component';
import {NewsEcosystemesComponent} from './ecosystemes/news-ecosystemes/news-ecosystemes.component';
import {EnteteMinistereComponent} from './entete-ministere/entete-ministere.component';
import {ShowappelComponent} from './showappel/showappel.component';
import {ShowResultComponent} from './show-result/show-result.component';
import {ShowEcosystemeArticleComponent} from './ecosystemes/show-ecosysteme-article/show-ecosysteme-article.component';
import {PostulerComponent} from './postuler/postuler.component';

export const routes: Routes = [
  {path: 'accueil', component: HomeIsepComponent},
  {path: 'apropos/nousconnaitre', component: NousConnaitreComponent},
  {path: 'apropos/missionsetambitions', component: MissionsEtAmbitionsComponent},
  {path: 'apropos/histoireetpatrimoine', component: HistoireEtPatrimoineComponent},
  {path: 'apropos/organigramme', component: OrganigrammeComponent},
  {path: 'formation/conditionsadmission', component: ConditionsAdmissionComponent},
  {path: 'formation/listeadmis', component: ListeAdmisComponent},
  {path: 'formation/filieres/metiersautomobiles', component: MetiersAutomobilesComponent},
  {path: 'formation/filieres/metiersdutic', component: MetiersDuTicComponent},
  {path: 'nosetudiants', component: NosEtudiantsComponent},
  {path: 'alumni', component: AlumniComponent},
  {path: 'ecosystemes', component: EcosystemesComponent},
  {path: '', redirectTo: 'accueil', pathMatch: 'full'},
  {path: 'news', component: NewsComponent},
  {path: 'admin', component: EspaceAdminComponent},
  {path: 'showarticle/:id', component: ShowArticleComponent},
  {path: 'showdepartement/:id', component: ShowDepartementComponent},
  {path: 'showfiliere/:id', component: ShowFiliereComponent},
  {path: 'formation', component: FormationIsepComponent},
  {path: 'firstheader', component: FirstHeaderComponent},
  {path: 'header2', component: NewHeaderComponent},
  {path: 'allocution', component: AllocutionsComponent},
  {path: 'formation/certifications', component: CertificationsComponent},
  {path: 'ecosystemes/partenaires', component: PartenairesComponent},
  {path: 'ecosystemes/news-ecosystemes', component: NewsEcosystemesComponent},
  {path: 'ecosystemes/evenements', component: EvenementsComponent},
  {path: 'ecosystemes/show-article-eco/:id', component: ShowEcosystemeArticleComponent},
  {path: 'appelsdoffres', component: AppelsDOffresComponentComponent},
  {path: 'home/ecosystemes/evenement/showevents/:id', component: ShoweventsComponent},
  {path: 'home/ecosystemes/partenaires/showpartenaire/:id', component: ShowpartenaireComponent},
  {path: 'ecosystemes/contact-us', component: ContactUsComponent},
  {path: 'entete', component: EnteteMinistereComponent},
  {path: 'appel/:id', component: ShowappelComponent},
  {path: 'search/:tagName', component: ShowResultComponent},
  {path: 'postuler', component: PostulerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
