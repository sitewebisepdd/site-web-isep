import { Component, OnInit } from '@angular/core';
import {IBox, IInfoWindowOptions, IMapOptions, MarkerTypeId} from 'angular-maps';
import IViewOptions = Microsoft.Maps.IViewOptions;

@Component({
  selector: 'app-new-header',
  templateUrl: './new-header.component.html',
  styleUrls: ['./new-header.component.css']
})
export class NewHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
