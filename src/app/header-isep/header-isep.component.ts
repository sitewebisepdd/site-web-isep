import { Component, OnInit } from '@angular/core';
import {Departement} from '../models/departement';
import {DepartementService} from '../services/departement.service';
import {Filiere} from '../models/filiere';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Tag} from '../models/tag';
import {Article} from '../models/article';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header-isep',
  templateUrl: './header-isep.component.html',
  styleUrls: ['./header-isep.component.css'],
  providers: [DepartementService]
})
export class HeaderIsepComponent implements OnInit {

  d: Departement;
  f: Filiere;
  t: Tag;
  tagName: string;
  departements: Departement[];
  departement: Departement;
  constructor(private departementService: DepartementService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router )
  {this.t = new Tag; }

  ngOnInit() {
    this.departement = new Departement();
    this.departements = this.departementService.departements;
    this.getDepartements();
    console.log('');
  }

  search(tag: string) {
    window.location.reload(true);
    this.router.navigate(['search', tag]);
  }
  private getDepartements() {
    this.departementService.getDepartements().subscribe(
      departements => {
        for (let i = 0; i < departements.length; i++) {
          this.departementService.getFilieresByDepartement(departements[i].id).subscribe(
            filieres => departements[i].filieres = filieres
          );
        }
        this.departements = departements;
        console.log(this.departements);
      },
      error => {
        console.log(error);
      }
    );
  }
}
