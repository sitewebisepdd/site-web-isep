import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Message} from '../models/message';
import {MessageService} from '../services/message.service';
import {Router} from '@angular/router';
import {MediaService} from '../services/media.service';
import {Media} from '../models/media';
import {AppelDOffreService} from '../services/appel-d-offre.service';
import {AppelDOffre} from '../models/appel-d-offre';
import {FileUploader} from 'ng2-file-upload';


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
@Component({
  selector: 'app-postuler',
  templateUrl: './postuler.component.html',
  styleUrls: ['./postuler.component.css'],
  providers: [MessageService, MediaService, AppelDOffreService]
})
export class PostulerComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: URL});
  submitting: boolean;
  formMessage: FormGroup;
  @Output()back: EventEmitter<Message> = new EventEmitter();
  m: Message;
  medias: Media[];
  alerte: string;
  messages: Message[];
  message: Message;
  a: AppelDOffre;
  appelsDoffres = [];
  appel: AppelDOffre;
  feedBack(mes: Message): void {
    this.messages.push(mes);
  }
  ngOnInit(): void {
    this.message = new Message();
    /*this.users = this.userService.users;
    this.getUsers();*/
    this.appel = new AppelDOffre();
    this.appelsDoffres = this.appelDoffreService.appelDOffres;
    this.getArticles();

  }
  private getArticles() {
    this.appelDoffreService.getAppelDOffres().subscribe(
      articles => {
        this.appelsDoffres = articles;
        console.log(this.appelsDoffres);
      },
      error => {
        console.log(error);
      }
    );
  }
  public fileDropped(event: any) {

    const $this = this;
    for (const item of event){
      const media = new Media;
      const reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        media.url = reader.result.toString().split(',')[1];
        media.type = item.name.split('.')[item.name.split('.').length - 1];
        $this.medias.push(media);
      };
    }

    this.m.medias = this.medias;
  }


  constructor(private fB: FormBuilder,
              private messageService: MessageService,
              private router: Router,
              private appelDoffreService: AppelDOffreService
  ) {
    this.submitting = false;
    this.m = new Message();
    this.m.medias = [];
    this.medias = [];
    this.formMessage = this.fB.group({
      'corps': ['', Validators.required],
      'emailEmetteur': ['', Validators.required],
      'objet': ['', Validators.required],
      'nameEmetteur': [''],
      'phoneEmetteur': [''],
      'companyEmetteur': [''],
    });

  }

  onSubmit() {
    this.submitting = true;
    if (this.m.id === undefined) {
      this.m.emailDestinataire = 'massd28@gmail.com';
      this.m.lu = false;
      this.m.received = true;
      console.log(this.m.corps);
      console.log(this.m.emailEmetteur);
      console.log(this.m.medias);
      this.messageService.postuler(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.submitting = false;
          this.alerte = 'Envoi réussi!!!';
          setTimeout(
            () => this.alerte = undefined,
            2000
          );
          this.router.navigate(['postuler']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.messageService.editMessage(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.submitting = false;
          this.alerte = 'Envoi réussi!';
          setTimeout(
            () => this.alerte = undefined,
            2000
          );
          this.router.navigate(['postuler']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

}
