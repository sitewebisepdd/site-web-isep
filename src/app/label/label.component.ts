import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Label} from '../models/label';
import {ArticleService} from '../services/article.service';
import {Router} from '@angular/router';
import {LabelService} from '../services/label.service';

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.css'],
  providers: [LabelService]
})
export class LabelComponent implements OnInit {

  @Output()back: EventEmitter<Label> = new EventEmitter();
  l: Label;
  labels = [];
  label: Label;

  constructor(private labelService: LabelService, private router: Router ) { }

  ngOnInit() {
    this.label = new Label();
    // this.labels = this.labelService.labels;
    this.getLabels();
    console.log('After getLabels()');
  }

  private getLabels() {
    this.labelService.getLabels().subscribe(
      labels => {
        this.labels = labels;
        console.log(this.labels);
      },
      error => {
        console.log(error);
      }
    );
  }
}
