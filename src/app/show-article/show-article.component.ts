import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../models/article';
import {ArticleService} from '../services/article.service';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
@Component({
  selector: 'app-show-article',
  templateUrl: './show-article.component.html',
  styleUrls: ['./show-article.component.css'],
  providers: [ArticleService]
})
export class ShowArticleComponent implements OnInit {
  @Output() back: EventEmitter<Article> = new EventEmitter();
  article: Article;
  a: Article;
  arc: Article;
  articles: Article[];
  idd: number;
  constructor(private articleService: ArticleService,
              private route: ActivatedRoute,
              private router: Router ) {}
  ngOnInit(): void {
    this.idd = this.route.snapshot.params['id'];
    this.getArticles();
  }
  getArticles(): void {
    this.articleService.getArticleById(this.route.snapshot.params['id']).subscribe(
      a => {
          a.images = [];
          a.videos = [];
          a.documents = [];
          this.articleService.getMediasByArticle(a.id).subscribe(
            medias => {
              for (let i = 0; i < medias.length; i++) {
                if (medias[i].type === 'PNG' || medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                  a.images.push(medias[i]);
                }
                if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                  a.videos.push(medias[i]);
                }
                if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                  a.documents.push(medias[i]);
                }
              }
            }
          );
        this.article = a;
        document.getElementById('texte').innerHTML = a.corps;
      }
    );
    this.articleService.getArticles().subscribe(
      articles => {
        this.articles = articles;
        console.log(this.articles);
      },
      error => {
        console.log(error);
      }
    );
  }
}
