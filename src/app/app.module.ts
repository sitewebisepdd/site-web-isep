import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';

import {AppRoutingModule, routes} from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderIsepComponent } from './header-isep/header-isep.component';
import { FooterIsepComponent } from './footer-isep/footer-isep.component';
import { HomeIsepComponent } from './home-isep/home-isep.component';
import { SlidesComponent } from './home-isep/slides/slides.component';
import { SectionEtudiantsComponent } from './home-isep/section-etudiants/section-etudiants.component';
import { NousConnaitreComponent } from './a-propos/nous-connaitre/nous-connaitre.component';
import { MissionsEtAmbitionsComponent } from './a-propos/missions-et-ambitions/missions-et-ambitions.component';
import { OrganigrammeComponent } from './a-propos/organigramme/organigramme.component';
import { HistoireEtPatrimoineComponent } from './a-propos/histoire-et-patrimoine/histoire-et-patrimoine.component';
import { MetiersAutomobilesComponent } from './formation/filieres/metiers-automobiles/metiers-automobiles.component';
import { MetiersDuTicComponent } from './formation/filieres/metiers-du-tic/metiers-du-tic.component';
import { ConditionsAdmissionComponent } from './formation/conditions-admission/conditions-admission.component';
import { ListeAdmisComponent } from './formation/liste-admis/liste-admis.component';
import { NosEtudiantsComponent } from './nos-etudiants/nos-etudiants.component';
import { AlumniComponent } from './alumni/alumni.component';
import { EcosystemesComponent } from './ecosystemes/ecosystemes.component';
import {NewsComponent} from './news/news.component';
import {ViceHeaderComponent} from './vice-header/vice-header.component';
import { ViceFooterComponent } from './vice-footer/vice-footer.component';
import { ArticleComponent } from './article/article.component';
import { EspaceAdminComponent } from './espace-admin/espace-admin.component';
import {ServeurService} from './services/serveur.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ShowArticleComponent } from './show-article/show-article.component';
import { ShowDepartementComponent } from './show-departement/show-departement.component';
import {ShowFiliereComponent} from './show-departement/show-filiere/show-filiere.component';
import { FormationIsepComponent } from './formation-isep/formation-isep.component';
import { ContainBrotherComponent } from './contain-brother/contain-brother.component';
import { ContainBrother2Component } from './contain-brother2/contain-brother2.component';
import { FirstHeaderComponent } from './first-header/first-header.component';
import { NewHeaderComponent } from './new-header/new-header.component';
import { AllocutionsComponent } from './allocutions/allocutions.component';
import { ShowEcosystemeArticleComponent } from './ecosystemes/show-ecosysteme-article/show-ecosysteme-article.component';
import { CertificationsComponent } from './formation/certifications/certifications.component';
import { PartenairesComponent } from './ecosystemes/partenaires/partenaires.component';
import { EvenementsComponent } from './ecosystemes/evenements/evenements.component';
import { AppelsDOffresComponentComponent } from './appels-doffres-component/appels-doffres-component.component';
import { ShowAppelsComponent } from './appels-doffres-component/show-appels/show-appels.component';
import { ShowEvenementComponent } from './ecosystemes/evenements/show-evenement/show-evenement.component';
import { LabelComponent } from './label/label.component';
import {NgxSmartModalComponent, NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
import { ShoweventsComponent } from './showevents/showevents.component';
import { ShowappelComponent } from './showappel/showappel.component';
import { ShowpartenaireComponent } from './ecosystemes/showpartenaire/showpartenaire.component';
import {LogosPartenaireComponent} from './ecosystemes/logos-partenaire/logos-partenaire.component';
import {HeaderEcosystemesComponent} from './ecosystemes/header-ecosystemes/header-ecosystemes.component';
import {ContactUsComponent} from './ecosystemes/contact-us/contact-us.component';
import { EnteteMinistereComponent } from './entete-ministere/entete-ministere.component';
import {NewsEcosystemesComponent} from './ecosystemes/news-ecosystemes/news-ecosystemes.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { ShowResultComponent } from './show-result/show-result.component';
import {MapModule, MapAPILoader, BingMapAPILoaderConfig, BingMapAPILoader, WindowRef, DocumentRef, MapServiceFactory, BingMapServiceFactory} from 'angular-maps';
import {PostulerComponent} from './postuler/postuler.component';
import {FileUploadModule} from 'ng2-file-upload';

@NgModule({
  declarations: [
    AppComponent,
    HeaderIsepComponent,
    SlidesComponent,
    FooterIsepComponent,
    HomeIsepComponent,
    SectionEtudiantsComponent,
    NousConnaitreComponent,
    MissionsEtAmbitionsComponent,
    OrganigrammeComponent,
    HistoireEtPatrimoineComponent,
    MetiersAutomobilesComponent,
    MetiersDuTicComponent,
    ConditionsAdmissionComponent,
    ListeAdmisComponent,
    NosEtudiantsComponent,
    AlumniComponent,
    NewsComponent,
    ViceHeaderComponent,
    ViceFooterComponent,
    ArticleComponent,
    EspaceAdminComponent,
    ShowArticleComponent,
    ShowDepartementComponent,
    ShowFiliereComponent,
    FormationIsepComponent,
    ContainBrotherComponent,
    ContainBrother2Component,
    FirstHeaderComponent,
    NewHeaderComponent,
    AllocutionsComponent,
    ShowEcosystemeArticleComponent,
    CertificationsComponent,
    PartenairesComponent,
    EvenementsComponent,
    AppelsDOffresComponentComponent,
    ShowAppelsComponent,
    ShowEvenementComponent,
    LabelComponent,
    ShoweventsComponent,
    ShowappelComponent,
    EcosystemesComponent,
    ShowpartenaireComponent,
    LogosPartenaireComponent,
    HeaderEcosystemesComponent,
    ContactUsComponent,
    EnteteMinistereComponent,
    ContactUsComponent,
    NewsEcosystemesComponent,
    ShowResultComponent,
    PostulerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FileUploadModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    NgxSmartModalModule.forRoot(),
    MapModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_KEY'
    })
  ],
  providers: [
    ServeurService,
    NgxSmartModalService,
    {
      provide: MapAPILoader, deps: [], useFactory: MapServiceProviderFactory
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function MapServiceProviderFactory() {
  let bc: BingMapAPILoaderConfig = new BingMapAPILoaderConfig();
  bc.apiKey ='ArYADcsR-gImyXCfG79AMom7iJH9Ptqdyoh7gM5HBSqsqEizr4ugxQgGGZGO_2lj';
  bc.branch = 'experimental';
  return new BingMapAPILoader(bc, new WindowRef(), new DocumentRef());
}
