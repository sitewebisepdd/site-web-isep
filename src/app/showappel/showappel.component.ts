import { Component, OnInit } from '@angular/core';
import {AppelDOffreService} from '../services/appel-d-offre.service';
import {AppelDOffre} from '../models/appel-d-offre';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-showappel',
  templateUrl: './showappel.component.html',
  styleUrls: ['./showappel.component.css'],
  providers: [AppelDOffreService]
})
export class ShowappelComponent implements OnInit {
  appel: AppelDOffre;
  constructor(private appelDOffresService: AppelDOffreService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getAppelDOffres();
  }

  getAppelDOffres(): void {
    this.appelDOffresService.getAppelDOffresById(this.route.snapshot.params['id']).subscribe(
      a => {
        a.images = [];
        a.videos = [];
        a.documents = [];
        this.appelDOffresService.getMediasByAppelDOffres(a.id).subscribe(
          medias => {
            for (let i = 0; i < medias.length; i++) {
              if (medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                a.images.push(medias[i]);
              }
              if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                a.videos.push(medias[i]);
              }
              if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                a.documents.push(medias[i]);
              }
            }
          }
        );
        this.appel = a;
        document.getElementById('texte').innerHTML = a.corps;
        console.log(a);

      }
    );
  }
}
