import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Departement} from '../models/departement';
import {DepartementService} from '../services/departement.service';
import {Filiere} from '../models/filiere';
import {error} from 'util';

@Component({
  selector: 'app-show-departement',
  templateUrl: './show-departement.component.html',
  styleUrls: ['./show-departement.component.css'],
  providers: [DepartementService]
})
export class ShowDepartementComponent implements OnInit {

  @Output() back: EventEmitter<Departement> = new EventEmitter();
  departement: Departement;
  a: Departement;
  departements: Departement[];
  idd: number;
  fi: Filiere;
  constructor(private departementService: DepartementService,
              private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.departement = new Departement();
    this.idd = this.route.snapshot.params['id'];
    this.departements = this.departementService.departements;
    this.getDepartements();
    console.log('');
  }
  getDepartements(): void {
    this.departementService.getDepartements().subscribe(
      departements => {
        for (let i = 0; i < departements.length; i++) {
          this.departementService.getFilieresByDepartement(departements[i].id).subscribe(
            filieres => departements[i].filieres = filieres
          );
        }
        this.departements = departements;
        console.log(this.departements);
      },
      error => {
        console.log(error);
      }
    );
    this.departementService.getDepartementById(this.route.snapshot.params['id']).subscribe(
      a => {
        this.departementService.getFilieresByDepartement(a.id).subscribe(
          filieres => a.filieres = filieres
        );
        this.departement = a;
        console.log(this.departement);
      },
      error => {
        console.log(error);
      }
    );
  }
}
