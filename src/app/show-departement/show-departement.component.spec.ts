import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDepartementComponent } from './show-departement.component';

describe('ShowDepartementComponent', () => {
  let component: ShowDepartementComponent;
  let fixture: ComponentFixture<ShowDepartementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDepartementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDepartementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
