import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowFiliereComponent } from './show-filiere.component';

describe('ShowDepartementComponent', () => {
  let component: ShowFiliereComponent;
  let fixture: ComponentFixture<ShowFiliereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowFiliereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowFiliereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
