import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Departement} from '../../models/departement';
import {Filiere} from '../../models/filiere';
import {DepartementService} from '../../services/departement.service';
import {ActivatedRoute} from '@angular/router';
import {FiliereService} from '../../services/filiere.service';

@Component({
  selector: 'app-show-filiere',
  templateUrl: './show-filiere.component.html',
  styleUrls: ['./show-filiere.component.css'],
  providers: [FiliereService]
})
export class ShowFiliereComponent implements OnInit {

  @Output() back: EventEmitter<Departement> = new EventEmitter();
  filiere: Filiere;
  idd: number;
  constructor(private filiereService: FiliereService,
              private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.idd = this.route.snapshot.params['id'];
    this.getFilieres();
  }
  getFilieres(): void {
    this.filiereService.getFiliereById(this.route.snapshot.params['id']).subscribe(
      a => this.filiere = a
    );
  }
}

