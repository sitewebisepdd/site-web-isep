import {Injectable} from '@angular/core';
import {Event} from '../models/event';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServeurService} from './serveur.service';

@Injectable()
export class EventService {
  baseURL: string;
  events: any[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getEvents(): Observable <any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/events', {headers: httpHeaders});
  }
  getEventsPublies(): Observable <any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/events/publies', {headers: httpHeaders});
  }
  getEventById(id: number): Observable <any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/events/' + id, {headers: httpHeaders});
  }
  createEvent(e: Event): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/events', e, {headers: httpHeaders});
  }
  editEvent(e: Event): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/events/' + e.id, e, {headers: httpHeaders});
  }
  deleteEvent(e: Event): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/events/' + e.id, {headers: httpHeaders});
  }
}
