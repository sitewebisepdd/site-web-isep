import {Injectable} from '@angular/core';
import {Message} from '../models/message';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServeurService} from './serveur.service';
import {Label} from '../models/label';

@Injectable()
export class LabelService {
  baseURL: string;
  messages: Message[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getLabels(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/labels', {headers: httpHeaders});
  }
  getLabelById(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/labels/' + id, {headers: httpHeaders});
  }
  createLabel(l: Label): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/labels', l, {headers: httpHeaders});
  }
  editLabel(l: Label): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/labels/' + l.id, l, {headers: httpHeaders});
  }
  deleteLabel(l: Label): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/labels/' + l.id, {headers: httpHeaders, responseType: 'text'});
  }
}

