import {Injectable} from '@angular/core';
import {AppelDOffre} from '../models/appel-d-offre';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ServeurService} from './serveur.service';

@Injectable()
export class AppelDOffreService {
  baseURL: string;
  appelDOffres: AppelDOffre[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getAppelDOffres(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/appelDOffres', {headers: httpHeaders});
  }
  getAppelDOffresPublies(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/appelDOffres/publies', {headers: httpHeaders});
  }
  getAppelDOffresById(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/appelDOffres/' + id, {headers: httpHeaders});
  }
  createAppelDOffres(a: AppelDOffre): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/appelDOffres', a, {headers: httpHeaders});
  }
  editAppelDOffres(a: AppelDOffre): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/appelDOffres/' + a.id, a, {headers: httpHeaders});
  }
  deleteAppelDOffres(a: AppelDOffre): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/appelDOffres/' + a.id, {headers: httpHeaders});
  }


  getAppelDOffresByTag(nomTag: string): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/appelDOffres' + nomTag, {headers: httpHeaders});
  }
  getAppelDOffresByMediaId(idMedia: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/appelDOffres' + idMedia, {headers: httpHeaders});
  }

  getMediasByAppelDOffres(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/appelDOffres/' + id + '/medias', {headers: httpHeaders});
  }
}
