import {Injectable} from '@angular/core';

@Injectable()
export class HeaderSettingService {
  private menu: string;

  public getMenu(): string {
    return this.menu;
  }
  public setMenu(Value: string ){
    this.menu = Value;
  }
}
