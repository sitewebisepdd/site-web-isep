/*import {Injectable} from '@angular/core';
import {User} from '../models/User';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServeurService} from './serveur.service';

@Injectable()
export class UserService {
  baseURL: string;
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getUsers(): Observable<User[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/users', {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throwError(error));
  }
  getUserById(id: number): Observable<User> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/users/' + id, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throwError(error));
  }
  createUser(u: User): Observable<User> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/users', u, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throwError(error));
  }
  editUser(u: User): Observable<User> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/Users/' + u.id, u, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throwError(error));
  }
  deleteUser(u: User): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/users/' + u.id, {headers: httpHeaders}).map(
      response => response,
    ).catch(error => Observable.throwError(error));
  }
}
*/
