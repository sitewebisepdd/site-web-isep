import {Injectable} from '@angular/core';
import {Departement} from '../models/Departement';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ServeurService} from './serveur.service';

@Injectable()
export class DepartementService {
  baseURL: string;
  departements: Departement[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getDepartements(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/departements', {headers: httpHeaders});
  }
  getDepartementById(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/departements/' + id, {headers: httpHeaders});
  }
  createDepartement(d: Departement): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/departements', d, {headers: httpHeaders});
  }
  editDepartement(d: Departement): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/departements/' + d.id, d, {headers: httpHeaders});
  }
  deleteDepartement(d: Departement): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/departements/' + d.id, {headers: httpHeaders});
  }

  getFilieresByDepartement(idDepartement: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/departements/' + idDepartement + '/filieres', {headers: httpHeaders});
  }
}
