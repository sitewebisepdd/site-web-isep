import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ServeurService} from './serveur.service';
import {Allocution} from '../models/allocution';
import {Observable} from 'rxjs';


@Injectable()
export class AllocutionService {
  baseURL: string;
  allocutions: Allocution[];

  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getAloocutions(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/allocutions', {headers: httpHeaders});
  }
  getRandomAllocution(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/allocution', {headers: httpHeaders});
  }
}
