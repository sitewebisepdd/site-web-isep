import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViceHeaderComponent } from './vice-header.component';

describe('ViceHeaderComponent', () => {
  let component: ViceHeaderComponent;
  let fixture: ComponentFixture<ViceHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViceHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViceHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
