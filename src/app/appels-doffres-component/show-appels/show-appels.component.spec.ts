import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAppelsComponent } from './show-appels.component';

describe('ShowAppelsComponent', () => {
  let component: ShowAppelsComponent;
  let fixture: ComponentFixture<ShowAppelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAppelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAppelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
