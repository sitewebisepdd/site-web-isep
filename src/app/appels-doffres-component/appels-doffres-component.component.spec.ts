import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppelsDOffresComponentComponent } from './appels-doffres-component.component';

describe('AppelsDOffresComponentComponent', () => {
  let component: AppelsDOffresComponentComponent;
  let fixture: ComponentFixture<AppelsDOffresComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppelsDOffresComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppelsDOffresComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
